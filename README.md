# PDAM Padang API Reverse Engineering

API PDAM Kota Padang hasil reverse engineering android apps ditulis dalam bahasa Python.

## Installasi

1. clone repository 
    ```shell script
    $ git clone https://gitlab.com/wakataw/pdam-padang-api-reverse-engineering.git
    ```
2. masuk ke direktori
    ```shell script
    $ cd pdam-padang-api-reverse-engineering
    ```
3. Install menggunakan `pip` atau `setup.py`
    ```shell script
    $ python setup.py install
    
    # atau
    
    $ pip install . 
    ```

## Penggunaan

### Inisiasi Objek 

```python
from pdampadang import Pdam

pdam = Pdam()
```

### Registrasi

Untuk dapat mengirimkan request melalui API, kita harus memiliki user terdaftar terlebih dahulu. Cara registrasi adalah sebagai berikut:

```python
from pdampadang import Pdam

pdam = Pdam()

result = pdam.register(email='test@test.com', name='Donald J. Trump', birth_date='19900101')
print(result)
```

Output dari script di atas adalah username dan kode aktivasi dalam format json. Kode aktivasi juga dikirim melalui email.

```json
{
    "id_user": "1234",
    "kode_aktifasi": "987654321"
}
```

`id_user` dan `kode_aktifasi` ini akan digunakan untuk aktivasi akun.


### Aktivasi Akun

```python
# kode sebelumnya

activation_status = pdam.activate(id_user=1234, activation_code='123', password='SuperSecret')
print(activation_status) 
# output True jika berhasil, False jika gagal
```

### Otentifikasi

PDAM API menggunakan JWT untuk otentifikasi request yang diterima.
Token disertakan dengan parameter CLIENT-TOKEN pada header setiap http requests. 
Untuk mendapatkan token, client mengirim kan requests dengan menyertakan email, password, 
dan secret (digunakan untuk encode dan decode JWT).

```python
client_token = pdam.auth(email='donald.j.trump@usa.gov', password='SuperSecret')
```

method akan mengembalikan nilai token yang diterima dari server yang selanjutnya akan digunakan
saat mengirim http requests. Selain digunakan untuk otentifikasi http requets, 
token yang diterima dari server juga memuat payload profile yang disimpan dalam objek `pdam.user` setelah didecode.

### User Profile

Merupakan hasil decode JWT, disimpan di objek `pdam.user` (`PdamUser` class) yang memiliki property sebagai berikut:

- `pdam.user.iss`: Versi API PDAM
- `pdam.user.iat`: unix time, kemungkinan waktu server saat generate payload.
- `pdam.user.aud`: Unit PDAM
- `pdam.user.id_user`: ID User 
- `pdam.user.email`: email user
- `pdam.user.nama`: Nama User
- `pdam.user.photo_profile`: URL Photo Profile
- `pdam.user.idpelanggan`: Daftar ID Pelanggan, hasil manual input oleh user
- `pdam.user.idrole`: Kode ID Role, belum diketahui kegunaannya
- `pdam.user.ipaddres`: Kemungkinan IP Address dari server

### Profile

Informasi sederhana user (email, id user, id pelanggan, dan tipe login).

```python
print(pdam.get_user_info())
```

output

```json
{
  "id_user": "6975", 
  "email": "rob.wicaksono@gmail.com", 
  "idpelanggan": "", 
  "type_login": "LOCAL LOGIN"
}
```

### Cek tagihan

```python
tagihan = pdam.get_invoice(customer_code='01.002.003')
```

output adalah daftar tagihan yang belum lunas dengan format json, 
output akan kosong jika tidak ada tagihan yang belum dibayar.

contoh output:

```json
[
    {
        "administrasi": "3000",
        "alamat": "White House",
        "bulan": "07",
        "danameter": "7500",
        "denda": "5000",
        "disisihkan": "0",
        "hargaair": "72400",
        "kodegoltarif": "2C",
        "kodepelanggan": "01.002.003",
        "makhir": "3071",
        "materai": "0",
        "mawal": "3047",
        "minpemakaianair": "10",
        "nama": "DONALD J. TRUMP",
        "pemakaian": "24",
        "sampah": "7500",
        "tahun": "2019",
        "total": "95400"
    }
]
```

### Cek Riwayat Tagihan

Menampilkan riwayat tagihan selama 10 bulan terakhir.

```python
history_invoice = pdam.get_invoice_history(customer_code='01.002.003')
```

output berupa history tagihan dengan format json

```json
[
    {
        "alamat": "White House",
        "bulan": "1",
        "jumlahbayar": "66600",
        "kodegoltarif": "2C",
        "makhir": "2967",
        "mawal": "2954",
        "nama": "DONALD J. TRUMP",
        "namaloket": "KANTOR PUSAT",
        "pemakaian": "13",
        "tahun": "2019",
        "tglbayar": "04/04/2019:14:47:29"
    },
    {
        "alamat": "White House",
        "bulan": "2",
        "jumlahbayar": "68000",
        "kodegoltarif": "2C",
        "makhir": "2982",
        "mawal": "2967",
        "nama": "DONALD J. TRUMP",
        "namaloket": "KANTOR PUSAT",
        "pemakaian": "15",
        "tahun": "2019",
        "tglbayar": "04/04/2019:14:47:30"
    },

    ...
]
```

### Data Volume Pemakaian

Menampilkan data volume pemakaian selama tahun tertentu.

```python
usage = pdam.get_usage_histo(customer_code='01.002.003', year=2019)
```

output berupa riwayat penggunaan perbulan selama satu tahun

```json
[
    {
        "pemakaian": "13",
        "periode": "01/2019"
    },
    {
        "pemakaian": "15",
        "periode": "02/2019"
    },
    {
        "pemakaian": "15",
        "periode": "03/2019"
    },
    {
        "pemakaian": "18",
        "periode": "04/2019"
    },
    {
        "pemakaian": "14",
        "periode": "05/2019"
    },
    {
        "pemakaian": "18",
        "periode": "06/2019"
    },
    {
        "pemakaian": "24",
        "periode": "07/2019"
    }
]
```

## Limitasi

Masih terdapat beberapa API hasil reverse engineering yang tidak dikembangkan
karena tidak terlalu terlalu diperlukan antara lain:

- `GET /Gangguan/getAllGangguan`: data daftar gangguan namun tidak update.
- `GET /Informasi/getInformasi`: data informasi/berita PDAM, tidak update.
- `GET /Config/getConfig`: mendapatkan data configurasi aplikasi.
- `GET /MenuPelanggan/getMenuAll`: mendapatkan list menu untuk tampilan di aplikasi.

## Legal

Kode yang tersedia tidak terafiliasi, diotorisasi, dimaintain, disponsori dan/atau diendorse oleh PDAM Padang atau pihak
lain yang terkait. Kode ini bersifat tidak resmi dan independent. Risiko penggunaan ditanggung masing-masing.

## Lisensi

Kode yang tersedia direlease dibawah lisensi MIT