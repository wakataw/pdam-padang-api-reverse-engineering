import requests
import jwt

from secrets import token_urlsafe
from . import exceptions as pdam_exceptions


class PdamUser(object):
    def __init__(self, jwt_token, secret):
        profile = jwt.decode(
            jwt_token,
            secret,
            algorithms=['HS256'],
            verify=False
        )

        for k, v in profile.items():
            setattr(self, k, v)

    def __str__(self):
        return str(self.__dict__)


class Pdam(object):
    def __init__(self):
        self.client_token = None
        self.user = None
        self.secret = None

    @staticmethod
    def path(path):
        return "http://mobile.pdampadang.co.id:8082/new_mobilepdampdg/apiservice/index.php/mobile{}".format(path)

    def post(self, path, **kwargs):
        url = self.path(path)

        resp = requests.post(
            url,
            data=kwargs,
            headers={
                'content-type': 'application/x-www-form-urlencoded',
                'user-agent': 'okhttp/3.8.0'
            }
        )

        if resp.status_code == 511:
            raise pdam_exceptions.PdamUnauthenticatedException(str("{}: {}".format(resp.status_code, resp.text)))
        elif resp.status_code != 200:
            raise pdam_exceptions.PdamServerErrorException(str("{}: {}".format(resp.status_code, resp.text)))

        return resp.json()

    def register(self, email, name, birth_date):
        data = self.post(
            path='/User/regiterUser',
            email=email,
            nama=name,
            tgl_lahir=birth_date
        )

        if data['id_user'] == '0':
            raise pdam_exceptions.PdamUserAlreadyRegisteredException

        return data

    def activate(self, id_user, activation_code, password):
        data = self.post(
            path='/User/updateRegister',
            id_user=id_user,
            password=password,
            kode_aktifasi=activation_code
        )

        if data['id_user'] == id_user:
            return True
        else:
            return False

    def auth(self, email, password):
        self.client_token = None
        self.secret = token_urlsafe(16)

        self.client_token = self.post(
            path='/User/login',
            email=email,
            password=password,
            token=self.secret
        )

        self.user = PdamUser(self.client_token, self.secret)

        return True

    def get(self, path, **kwargs):
        resp = requests.get(
            self.path(path),
            params=kwargs,
            headers={
                'CLIENT-TOKEN': self.client_token,
                'user-agent': 'okhttp:/3.8.0'
            }
        )

        if resp.status_code != 200:
            raise pdam_exceptions.PdamServerErrorException(str("{}: {}".format(resp.status_code, resp.text)))

        return resp.json()

    def get_invoice(self, customer_code):

        return self.get(
            path='/Pelanggan/getTagihanRekening',
            kodepelanggan=customer_code
        )

    def get_invoice_history(self, customer_code):

        return self.get(
            path='/Pelanggan/historyPelanggan',
            kodepelanggan=customer_code
        )

    def get_usage_histo(self, customer_code, year):
        return self.get(
            path='/Pelanggan/grafikPemakaianAir',
            kodepelanggan=customer_code,
            tahun=year
        )

    def get_user_info(self):
        if self.user is None:
            raise pdam_exceptions.PdamUnauthenticatedException

        return self.post(
            path='/User/userDetail',
            id_user=self.user.id_user
        )
