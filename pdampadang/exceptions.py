class PdamServerErrorException(Exception):
    pass


class PdamUserAlreadyRegisteredException(Exception):
    pass


class PdamGeneralException(Exception):
    pass


class PdamUnauthenticatedException(Exception):
    pass
