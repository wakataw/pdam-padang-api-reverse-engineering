from setuptools import setup

setup(
    name='pdam-padang',
    version='0.1',
    packages=['pdampadang'],
    url='https://gitlab.com/wakataw/pdam-padang-api-reverse-engineering',
    install_requires=['requests'],
    test_requires=['python-dotenv'],
    license='MIT',
    author='Agung Pratama',
    author_email='agungpratama1001@gmail.com',
    description='PDAM Padang API Reverse Engineering from Android APK'
)
