import unittest
import os

from dotenv import load_dotenv
from pdampadang import Pdam
from pdampadang import exceptions


class TestPdam(unittest.TestCase):

    def setUp(self):
        load_dotenv()
        self.pdam = Pdam()
        self.email = os.getenv('PDAM_EMAIL')
        self.password = os.getenv('PDAM_PASSWORD')
        self.customer_code = os.getenv('PDAM_CUSTOMER_CODE')
        self.pdam.auth(self.email, self.password)

    def test_auth(self):
        pdam = Pdam()
        status = pdam.auth(email=self.email, password=self.password)

        self.assertTrue(pdam.client_token is not None)
        self.assertEqual(status, True)

    def test_auth_wrong_failed(self):
        pdam = Pdam()

        self.assertRaises(exceptions.PdamServerErrorException, pdam.auth, 'kimjonun@nk.gov', '123')
        self.assertTrue(pdam.client_token is None)

    def test_get_invoice(self):
        tagihan = self.pdam.get_invoice(customer_code=self.customer_code)

        self.assertIsInstance(tagihan, list)

    def test_get_invoice_history(self):
        tagihan = self.pdam.get_invoice(customer_code=self.customer_code)

        self.assertIsInstance(tagihan, list)

    def test_get_histogram_data(self):
        usage = self.pdam.get_usage_histo(customer_code=self.customer_code, year=2018)

        self.assertIsInstance(usage, list)
        self.assertIsInstance(usage[0], dict)

    def test_get_user_profile(self):
        profile = self.pdam.get_user_info()

        self.assertIsInstance(profile, dict)


if __name__ == '__main__':
    unittest.main()
